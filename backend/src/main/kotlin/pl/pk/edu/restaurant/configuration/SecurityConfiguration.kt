package pl.pk.edu.restaurant.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import pl.pk.edu.restaurant.auth.JwtAuthenticationFilter

@Configuration
@EnableMethodSecurity
class SecurityConfiguration {

    @Bean
    fun securityFilterChain(http: HttpSecurity, jwtAuthFilter: JwtAuthenticationFilter,
                            authenticationProvider: AuthenticationProvider): SecurityFilterChain {
        http.cors {  }
            .csrf { it.disable() }
            .authorizeHttpRequests {
                // enable authentication on all requests except for auth
                it.requestMatchers("/api/auth/*", "/swagger-ui/**", "/v3/api-docs/**").permitAll()
                    .requestMatchers(HttpMethod.GET, "/api/dishes").permitAll()
                    .anyRequest().authenticated()

                // disable authentication, for testing purposes
//                it.anyRequest().permitAll()
            }
            .sessionManagement { it.sessionCreationPolicy(SessionCreationPolicy.STATELESS) }
            .authenticationProvider(authenticationProvider)
            .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter::class.java)
            .exceptionHandling { it.authenticationEntryPoint(Http401UnauthorizedEntryPoint()) }

        return http.build()
    }
}