package pl.pk.edu.restaurant.auth

import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import pl.pk.edu.restaurant.database.service.UserService

@Component
class JwtAuthenticationFilter(private val jwtService: JwtService,
                              private val userService: UserService) : OncePerRequestFilter() {

    companion object {
        private const val TOKEN_PREFIX = "Bearer "
    }

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse,
        filterChain: FilterChain) {
        val authHeader = request.getHeader("Authorization")
        if (authHeader == null || !authHeader.startsWith(TOKEN_PREFIX, false)) {
            filterChain.doFilter(request, response)
            return
        }

        val jwtToken = authHeader.substring(TOKEN_PREFIX.length)
        if (!jwtService.validateToken(jwtToken)) {
            filterChain.doFilter(request, response)
            return
        }

        val email = jwtService.extractEmail(jwtToken)
        if (email != null && SecurityContextHolder.getContext().authentication == null) {
            val userDetails = userService.loadUserByUsername(email)
            if (jwtService.isTokenUserInfoValid(jwtToken, userDetails)) {
                val authToken =
                    UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
                authToken.details = WebAuthenticationDetailsSource().buildDetails(request)
                SecurityContextHolder.getContext().authentication = authToken
            }
        }
        filterChain.doFilter(request, response)
    }
}