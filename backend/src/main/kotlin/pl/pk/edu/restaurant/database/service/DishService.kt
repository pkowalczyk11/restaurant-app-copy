package pl.pk.edu.restaurant.database.service

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import pl.pk.edu.restaurant.database.model.Dish
import pl.pk.edu.restaurant.database.repository.DishRepository
import pl.pk.edu.restaurant.exceptions.NotFoundException

@Service
class DishService(private val dishRepository: DishRepository) {

    fun getAllDishes(): List<Dish> =
        dishRepository.findAll()

    fun getDishById(dishId: Int): Dish? =
        dishRepository.findByIdOrNull(dishId) ?: throw NotFoundException("No dish with $dishId id")

    fun addDish(dish: Dish): Dish {
        return dishRepository.save(dish)
    }

    fun deleteDishById(dishId: Int) {
        if (!dishRepository.existsById(dishId)) {
            throw NotFoundException("No dish with $dishId id")
        }
        dishRepository.deleteById(dishId)
    }

    fun isDishNameAvailable(name: String): Boolean = !dishRepository.existsByName(name)
}