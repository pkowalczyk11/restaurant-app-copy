package pl.pk.edu.restaurant.api.controller

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.pk.edu.restaurant.api.ErrorResponse
import pl.pk.edu.restaurant.api.model.UserInfo
import pl.pk.edu.restaurant.api.model.request.LoginRequest
import pl.pk.edu.restaurant.api.model.request.SignUpRequest
import pl.pk.edu.restaurant.api.model.response.AuthenticationResponse
import pl.pk.edu.restaurant.auth.JwtService
import pl.pk.edu.restaurant.database.model.User
import pl.pk.edu.restaurant.database.service.UserService

@RestController
@RequestMapping("api/auth", produces = [MediaType.APPLICATION_JSON_VALUE],
    consumes = [MediaType.APPLICATION_JSON_VALUE])
@CrossOrigin
class AuthController(private val userService: UserService,
                     private val jwtService: JwtService,
                     private val authenticationManager: AuthenticationManager) {

    @Operation(summary = "Register new user")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "201", description = "User has been created",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = UserInfo::class))]),
            ApiResponse(responseCode = "403", description = "Email not available",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "500", description = "Internal server error",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))])
        ])
    @PostMapping("/signup")
    fun registerUser(@RequestBody signUpRequest: SignUpRequest): ResponseEntity<Any> {
        if (!userService.isEmailAvailable(signUpRequest.email)) {
            return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(ErrorResponse("Email not available!"))
        }
        val user = userService.addUser(signUpRequest.toDbUser())
        return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(UserInfo.fromDbUser(user))
    }

    @Operation(summary = "Log in")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "Logged in, token created",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = AuthenticationResponse::class))]),
            ApiResponse(responseCode = "401", description = "Problem with credentials",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "500", description = "Internal server error",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))])
        ])
    @PostMapping("/login")
    fun loginUser(@RequestBody loginRequest: LoginRequest): ResponseEntity<Any> {
        return try {
            val authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(loginRequest.email, loginRequest.password))
            SecurityContextHolder.getContext().authentication = authentication
            val user = authentication.principal as User
            val token = jwtService.generateToken(user)
            ResponseEntity.ok(AuthenticationResponse(user.id!!, token))
        } catch (e : AuthenticationException) {
            ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(ErrorResponse(e.message!!))
        }
    }
}