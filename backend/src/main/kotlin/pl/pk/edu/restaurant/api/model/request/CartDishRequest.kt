package pl.pk.edu.restaurant.api.model.request

data class CartDishRequest(val dishId: Int,
                           val quantity: Int)
