package pl.pk.edu.restaurant.api.model.response

data class AuthenticationResponse(val id: Int,
                                  val authToken: String)
