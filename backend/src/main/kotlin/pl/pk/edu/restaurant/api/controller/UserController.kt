package pl.pk.edu.restaurant.api.controller

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.pk.edu.restaurant.api.ErrorResponse
import pl.pk.edu.restaurant.api.model.UserInfo
import pl.pk.edu.restaurant.database.service.UserService

@RestController
@RequestMapping("api/users", produces = [MediaType.APPLICATION_JSON_VALUE])
@CrossOrigin
class UserController(private val userService: UserService) {

    @Operation(summary = "Get user data")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "User data returned",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = UserInfo::class))]),
            ApiResponse(responseCode = "404", description = "User not found",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))]),
            ApiResponse(responseCode = "500", description = "Internal server error",
                content = [Content(mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class))])
        ])
    @GetMapping("/{userId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getUser(@PathVariable userId: Int): ResponseEntity<UserInfo> {
        val user = userService.getById(userId)
        return ResponseEntity.ok(UserInfo.fromDbUser(user))
    }
}
