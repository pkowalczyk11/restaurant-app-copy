package pl.pk.edu.restaurant.exceptions

class NotFoundException(message: String) : RuntimeException(message)