package pl.pk.edu.restaurant.auth

import io.jsonwebtoken.Claims
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.SignatureAlgorithm.HS256
import io.jsonwebtoken.UnsupportedJwtException
import io.jsonwebtoken.io.Decoders
import io.jsonwebtoken.security.Keys
import io.jsonwebtoken.security.SignatureException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import java.security.Key
import java.time.Clock
import java.time.Instant
import java.util.Date


@Service
class JwtService(@Value("\${restaurantApp.auth.jwt.secretKey:secret}") private val secretKey: String,
                 @Value("\${restaurantApp.auth.jwt.expirationTimeSecs:1800}") private val expirationTime: Long) {

    companion object {
        private val log = LoggerFactory.getLogger(JwtService::class.java)
    }

    fun extractEmail(token: String): String? = extractClaim(token, Claims::getSubject)

    fun extractExpiration(token: String): Date? = extractClaim(token, Claims::getExpiration)

    fun <T> extractClaim(token: String, claimResolver: (Claims) -> T?): T? =
        claimResolver.invoke(extractAllClaims(token))

    fun generateToken(userDetails: UserDetails): String = generateToken(mapOf(), userDetails)

    fun generateToken(extraClaims: Map<String, Any>, userDetails: UserDetails): String =
        Jwts.builder()
            .setClaims(extraClaims)
            .setSubject(userDetails.username)
            .setIssuedAt(Date())
            .setExpiration(Date.from(Instant.now().plusSeconds(expirationTime)))
            .signWith(getSignInKey(), HS256)
            .compact()

    fun validateToken(token: String): Boolean {
        return try {
            Jwts.parserBuilder()
                .setSigningKey(getSignInKey())
                .build()
                .parseClaimsJws(token)
            true
        } catch (e: Exception) {
            when (e) {
                is SignatureException,
                is MalformedJwtException,
                is UnsupportedJwtException,
                is IllegalArgumentException -> log.warn("Invalid credentials for JWT token")
                is ExpiredJwtException -> log.warn("JWT token expired")
                else -> throw e
            }
            false
        }
    }

    fun isTokenUserInfoValid(token: String, userDetails: UserDetails): Boolean =
        extractEmail(token) == userDetails.username && !isTokenExpired(token)

    private fun isTokenExpired(token: String): Boolean = extractExpiration(token)?.before(Date()) ?: true

    private fun extractAllClaims(token: String): Claims =
        Jwts.parserBuilder()
            .setSigningKey(getSignInKey())
            .build()
            .parseClaimsJws(token).body

    private fun getSignInKey(): Key {
        val keyBytes = Decoders.BASE64.decode(secretKey)
        return Keys.hmacShaKeyFor(keyBytes)
    }
}