package pl.pk.edu.restaurant.database.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pl.pk.edu.restaurant.database.model.Dish

@Repository
interface DishRepository: JpaRepository<Dish, Int> {
    fun findByName(name: String): Dish?

    fun existsByName(name: String): Boolean
}