package pl.pk.edu.restaurant.api.model.request

import pl.pk.edu.restaurant.database.model.Dish

data class DishRequest(val name: String,
                       val description: String,
                       val price: Double) {

    fun toDbDish(): Dish = Dish(name, description, price)
}