package pl.pk.edu.restaurant.exceptions

class UnauthorizedException(message: String) : RuntimeException(message)