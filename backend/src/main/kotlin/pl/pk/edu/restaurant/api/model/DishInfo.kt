package pl.pk.edu.restaurant.api.model

import pl.pk.edu.restaurant.database.model.Dish


data class DishInfo(val name: String,
                    val description: String,
                    val price: Double,
                    val id: Int) {

    companion object {
        fun fromDbDish(dish: Dish): DishInfo = DishInfo(dish.name,
            dish.description, dish.price, dish.id!!)
    }
}