package pl.pk.edu.restaurant.database.service

import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.Test
import org.springframework.data.repository.findByIdOrNull
import pl.pk.edu.restaurant.database.repository.DishRepository
import pl.pk.edu.restaurant.exceptions.NotFoundException

class DishServiceTest {

    private val dishRepository = mockk<DishRepository>()
    private val dishService = DishService(dishRepository)

    @Test
    fun `should throw exception when trying to delete not existing dish`() {
        //given
        val dishId = 10
        every { dishRepository.existsById(dishId) }.returns(false)

        //when
        val exception = catchThrowable { dishService.deleteDishById(dishId) }

        //then
        assertThat(exception).isInstanceOf(NotFoundException::class.java)
        assertThat(exception as NotFoundException).hasMessage("No dish with $dishId id")
    }

    @Test
    fun `should throw exception when fetching not existing dish`() {
        //given
        val dishId = 10
        every { dishRepository.findByIdOrNull(dishId) }.returns(null)

        //when
        val exception = catchThrowable { dishService.getDishById(dishId) }

        //then
        assertThat(exception).isInstanceOf(NotFoundException::class.java)
        assertThat(exception as NotFoundException).hasMessage("No dish with $dishId id")
    }
}