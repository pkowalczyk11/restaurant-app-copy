package pl.pk.edu.restaurant.database.service

import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.Test
import org.springframework.data.repository.findByIdOrNull
import pl.pk.edu.restaurant.database.model.Cart
import pl.pk.edu.restaurant.database.model.CartState
import pl.pk.edu.restaurant.database.model.Dish
import pl.pk.edu.restaurant.database.model.DishInCart
import pl.pk.edu.restaurant.database.model.User
import pl.pk.edu.restaurant.database.repository.CartRepository
import pl.pk.edu.restaurant.database.repository.DishRepository
import pl.pk.edu.restaurant.exceptions.NotFoundException
import pl.pk.edu.restaurant.exceptions.UnauthorizedException

class CartServiceTest {

    private val dishRepository = mockk<DishRepository>()
    private val cartRepository = mockk<CartRepository>()
    private val cartService = CartService(cartRepository, dishRepository)

    @Test
    fun `should update dish quantity when dish exists in cart`() {
        //given
        val dishId = 10
        val dish = Dish("dish", "description", 15.0, id = dishId)
        val user = User("name", "surname", "test@abc.com", "password", false, 10)
        val cart = Cart(user, id = 10)
        cart.dishes.addLast(DishInCart(dish, cart, 15.0, 10, 10))
        every { cartRepository.findByUserAndState(user, CartState.PENDING) }.returns(cart)
        every { cartRepository.save(any()) }.returns(cart)
        val cartCaptor = slot<Cart>()

        //when
        cartService.updateDishQuantity(dishId, 15, user)

        //then
        verify { cartRepository.save(capture(cartCaptor)) }
        val capturedCart = cartCaptor.captured
        assertThat(capturedCart.dishes).hasSize(1)
        assertThat(capturedCart.dishes.first().dish.name).isEqualTo(dish.name)
        assertThat(capturedCart.dishes.first().quantity).isEqualTo(15)
    }

    @Test
    fun `should add dish to cart if does not already exist`() {
        //given
        val dishId = 10
        val dish = Dish("dish", "description", 15.0, id = dishId)
        val user = User("name", "surname", "test@abc.com", "password", false, 10)
        val cart = Cart(user, id = 10)
        every { dishRepository.findByIdOrNull(dishId) }.returns(dish)
        every { cartRepository.findByUserAndState(user, CartState.PENDING) }.returns(cart)
        every { cartRepository.save(any()) }.returns(cart)
        val cartCaptor = slot<Cart>()

        //when
        cartService.updateDishQuantity(dishId, 15, user)

        //then
        verify { cartRepository.save(capture(cartCaptor)) }
        val capturedCart = cartCaptor.captured
        assertThat(capturedCart.dishes).hasSize(1)
        assertThat(capturedCart.dishes.first().dish.name).isEqualTo(dish.name)
        assertThat(capturedCart.dishes.first().quantity).isEqualTo(15)
    }

    @Test
    fun `should create new cart when one does not already exist`() {
        //given
        val dishId = 10
        val dish = Dish("dish", "description", 15.0, id = dishId)
        val user = User("name", "surname", "test@abc.com", "password", false, 10)
        val cart = Cart(user, id = 10)
        every { dishRepository.findByIdOrNull(dishId) }.returns(dish)
        every { cartRepository.findByUserAndState(user, CartState.PENDING) }.returns(null)
        every { cartRepository.save(any()) }.returns(cart)
        val cartCaptor = slot<Cart>()

        //when
        cartService.updateDishQuantity(dishId, 15, user)

        //then
        verify { cartRepository.save(capture(cartCaptor)) }
        val capturedCart = cartCaptor.captured
        assertThat(capturedCart.dishes).hasSize(1)
        assertThat(capturedCart.dishes.first().dish.name).isEqualTo(dish.name)
        assertThat(capturedCart.dishes.first().quantity).isEqualTo(15)
    }

    @Test
    fun `should throw exception when cannot find dish`() {
        //given
        val dishId = 10
        val user = User("name", "surname", "test@abc.com", "password", false, 10)
        val cart = Cart(user, id = 10)
        every { dishRepository.findByIdOrNull(dishId) }.returns(null)
        every { cartRepository.findByUserAndState(user, CartState.PENDING) }.returns(cart)
        every { cartRepository.save(any()) }.returns(cart)

        //when
        val exception = catchThrowable { cartService.updateDishQuantity(dishId, 15, user) }

        //then
        assertThat(exception).isInstanceOf(NotFoundException::class.java)
        assertThat(exception as NotFoundException).hasMessage("Dish with $dishId id not found")
    }

    @Test
    fun `should change cart state to ordered`() {
        //given
        val cartId = 10
        val user = User("name", "surname", "test@abc.com", "password", false, 10)
        val cart = Cart(user, id = cartId)
        every { cartRepository.save(any()) }.returns(cart)
        every { cartRepository.findByIdOrNull(cartId) }.returns(cart)
        val cartCaptor = slot<Cart>()

        //when
        cartService.orderCart(cartId, 10)

        //then
        verify { cartRepository.save(capture(cartCaptor)) }
        val capturedCart = cartCaptor.captured
        assertThat(capturedCart.state).isEqualTo(CartState.ORDERED)
    }

    @Test
    fun `should throw exception when trying to order cart that belongs to different user`() {
        //given
        val cartId = 10
        val userId = 10
        val differentUserId = 15
        val user = User("name", "surname", "test@abc.com", "password", false, userId)
        val cart = Cart(user, id = cartId)
        every { cartRepository.save(any()) }.returns(cart)
        every { cartRepository.findByIdOrNull(cartId) }.returns(cart)

        //when
        val exception = catchThrowable { cartService.orderCart(cartId, differentUserId) }

        //then
        assertThat(exception).isInstanceOf(UnauthorizedException::class.java)
        assertThat(exception as UnauthorizedException).hasMessage("Cannot order cart that doesn't belong to a user")
    }
}