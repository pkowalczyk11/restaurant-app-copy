package pl.pk.edu.restaurant.api.controller

import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import pl.pk.edu.restaurant.api.ErrorResponse
import pl.pk.edu.restaurant.api.model.DishInfo
import pl.pk.edu.restaurant.api.model.request.DishRequest
import pl.pk.edu.restaurant.auth.AuthenticatedUserService
import pl.pk.edu.restaurant.database.model.Dish
import pl.pk.edu.restaurant.database.service.DishService

class DishControllerTest {

    private val authenticatedUserService = mockk<AuthenticatedUserService>()
    private val dishService = mockk<DishService>()
    private val dishController = DishController(dishService, authenticatedUserService)

    @Test
    fun `should return 403 status code when adding already existing dish`() {
        //given
        val dishRequest = DishRequest("name", "description", 10.0)
        every { dishService.isDishNameAvailable(dishRequest.name) }.returns(false)

        //when
        val response = dishController.addDish(dishRequest)

        //then
        assertThat(response.statusCode).isEqualTo(HttpStatus.FORBIDDEN)
        assertThat(response.body).isInstanceOf(ErrorResponse::class.java)
        assertThat((response.body as ErrorResponse).message).isEqualTo("Dish name not available!")
    }

    @Test
    fun `should return 201 status code when added new dish`() {
        //given
        val dishRequest = DishRequest("name", "description", 10.0)
        val dish = Dish(dishRequest.name, dishRequest.description, dishRequest.price, id = 10)
        every { dishService.isDishNameAvailable(dishRequest.name) }.returns(true)
        every { dishService.addDish(eq(dish)) }.returns(dish)
        val exceptedResponseBody = DishInfo(dish.name, dish.description, dish.price, dish.id!!)

        //when
        val response = dishController.addDish(dishRequest)

        //then
        assertThat(response.statusCode).isEqualTo(HttpStatus.CREATED)
        assertThat(response.body).isInstanceOf(DishInfo::class.java)
        val responseBody = response.body as DishInfo
        assertThat(responseBody).isEqualTo(exceptedResponseBody)
    }
}