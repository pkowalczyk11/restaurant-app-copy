import { Button, Container, TextField } from 'components';
import { useDishContext } from 'features/dish';
import { useUserContext } from 'features/user';
import { useFormik } from 'formik';
import { useNavigate } from 'react-router-dom';
import { FormContainer, Title } from 'screens/common';
import * as yup from 'yup';

type Values = {
  name: string;
  description: string;
  price: number | undefined;
};

const initialValues: Values = {
  name: '',
  description: '',
  price: undefined,
};

export function AddDish() {
  const { addDish, getDishes } = useDishContext();
  const { user } = useUserContext();

  const navigate = useNavigate();

  const validationSchema = yup.object().shape({
    name: yup.string().required('Name is required'),
    description: yup.string().required('Description is required'),
    price: yup.number().required('Price is required').min(1, 'Minimum price is 1$'),
  });

  const { values, errors, touched, handleChange, submitForm } = useFormik<Values>({
    initialValues,
    validationSchema,
    onSubmit: async () => {
      if (user && user.isAdmin) {
        const price = values.price ?? 0;
        const dish = { ...values, userId: user.userId, price };
        await addDish(dish);
        await getDishes();
        navigate('/');
      }
    },
  });

  return (
    <Container>
      <FormContainer>
        <Title type={3}>Add dish form</Title>
        <TextField
          label="Name"
          name="name"
          type="string"
          value={values.name}
          error={touched.name && !!errors.name}
          errorText={errors.name}
          onChange={handleChange('name')}
        />
        <TextField
          label="Description"
          name="description"
          type="string"
          value={values.description}
          error={touched.description && !!errors.description}
          errorText={errors.description}
          onChange={handleChange('description')}
        />
        <TextField
          label="Price"
          name="price"
          type="number"
          value={values.price}
          error={touched.price && !!errors.price}
          errorText={errors.price}
          onChange={handleChange('price')}
        />
      </FormContainer>
      <Button text="Add dish" onClick={submitForm} />
    </Container>
  );
}
