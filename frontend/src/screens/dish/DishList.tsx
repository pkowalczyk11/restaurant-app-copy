import { colors, Icon } from 'assets';
import { Container, Text, TextField } from 'components';
import { DISHES_PER_PAGE, useFilteredDishesContext } from 'features/dish';
import { Title } from 'screens/common';
import styled from 'styled-components';
import { Pagination, PaginationItem } from '@mui/material';

export function DishList() {
  const { filteredDishes, pageDishes, formik, currentPage, handlePageChange } =
    useFilteredDishesContext();

  return (
    <Container>
      <Title type={3}>Dish list</Title>
      <TextField
        label="Search"
        name="searchValue"
        type="string"
        placeholder="Enter dish name"
        value={formik.values.searchValue}
        onChange={formik.handleChange('searchValue')}
      />
      <div>
        {pageDishes.map((d) => (
          <Card key={d.id}>
            <Text.Subtitle color={colors.blueberry.blueberry550}>{d.name}</Text.Subtitle>
            <CardLine>
              <Icon.Book color={colors.gray.gray600} width={20} height={20} />
              <Text.Subtitle type={3} color={colors.gray.gray600}>
                {d.description}
              </Text.Subtitle>
            </CardLine>
            <CardLine>
              <Icon.Dollar color={colors.gray.gray600} width={20} height={20} />
              <Text.Subtitle type={3} color={colors.gray.gray600}>
                {d.price}
              </Text.Subtitle>
            </CardLine>
          </Card>
        ))}
      </div>
      <Pagination
        count={Math.ceil(filteredDishes.length / DISHES_PER_PAGE)}
        page={currentPage}
        onChange={(_e, page) => handlePageChange(page)}
        renderItem={(item) => (
          <PaginationItem
            {...item}
            component="button"
            onClick={() => handlePageChange(item.page ?? 1)}
          />
        )}
      />
    </Container>
  );
}

const Card = styled.div`
  border: 1px solid ${colors.gray.gray600};
  padding: 12px;
  border-radius: 4px;
  margin-bottom: 24px;
  width: 60%;
`;

const CardLine = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  column-gap: 8px;
  margin-top: 4px;
`;
