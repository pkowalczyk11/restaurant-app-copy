import styled from 'styled-components';
import { Text } from 'components';

export const Title = styled(Text.Title)`
  margin-bottom: 24px;
`;

export const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 8px;
  margin-bottom: 8px;
`;
