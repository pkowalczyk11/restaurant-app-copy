import { AuthProvider } from 'features/auth';
import { FilteredDishesProvider, DishProvider } from 'features/dish';
import { UserProvider } from 'features/user';
import { NavigationProvider } from 'navigation';

const App = () => {
  return (
    <div>
      <AuthProvider>
        <UserProvider>
          <DishProvider>
            <FilteredDishesProvider>
              <NavigationProvider />
            </FilteredDishesProvider>
          </DishProvider>
        </UserProvider>
      </AuthProvider>
    </div>
  );
};

export default App;
