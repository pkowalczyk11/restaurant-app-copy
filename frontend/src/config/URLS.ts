const API_BASE_URL = 'http://localhost:8080/api';

export const LOGIN_URL = API_BASE_URL + '/auth/login';
export const SIGNUP_URL = API_BASE_URL + '/auth/signup';

export const USER_URL = API_BASE_URL + '/users';

export const DISHES_URL = API_BASE_URL + '/dishes';
