import React from 'react';
import styled from 'styled-components';

export function Container({ children }: { children: React.ReactNode }) {
  return <StyledContainer>{children}</StyledContainer>;
}

const StyledContainer = styled.div`
  padding: 24px 40px;
  display: flex;
  flex-direction: column;
`;
