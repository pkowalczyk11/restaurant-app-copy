import { colors } from 'assets';
import styled from 'styled-components';
import { Text } from './Text';

type Props = {
  text: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
};
export function Button({ text, onClick }: Props) {
  return (
    <StyledButton onClick={onClick}>
      <Text.Body type={1} color={colors.base.cream}>
        {text}
      </Text.Body>
    </StyledButton>
  );
}

const StyledButton = styled.button`
  background-color: ${colors.blueberry.blueberry550};
  border: none;
  color: ${colors.base.cream};
  width: 500px;
  border-radius: 32px;
  padding: 10px 0;

  &:hover {
    background-color: ${colors.blueberry.blueberry450};
    cursor: pointer;
  }
`;
