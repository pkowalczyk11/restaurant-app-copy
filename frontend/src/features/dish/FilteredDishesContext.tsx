import { FormikProps, useFormik } from 'formik';
import React, { ReactNode, useContext, useEffect, useState } from 'react';
import { Dish } from 'types';
import { useDishContext } from './DishContext';

type Values = {
  searchValue: string;
};

type FilteredDishesContextState = {
  filteredDishes: Dish[];
  pageDishes: Dish[];
  formik: FormikProps<Values>;
  currentPage: number;
  handlePageChange: (page: number) => void;
};

const INITIAL_STATE = {
  filteredDishes: [],
  pageDishes: [],
  formik: {} as FormikProps<Values>,
  currentPage: 1,
  handlePageChange: () => {},
};

export const DISHES_PER_PAGE = 3;

export const FilteredDishesContext = React.createContext<FilteredDishesContextState>(INITIAL_STATE);

export function useFilteredDishesContext(): FilteredDishesContextState {
  return useContext<FilteredDishesContextState>(FilteredDishesContext);
}

export function FilteredDishesProvider({ children }: { children: ReactNode }) {
  const { dishes } = useDishContext();

  const [filteredDishes, setFilteredDishes] = useState<Dish[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);

  const lastDishIdx = currentPage * DISHES_PER_PAGE;
  const firstDishIdx = lastDishIdx - DISHES_PER_PAGE;

  const pageDishes = filteredDishes.slice(firstDishIdx, lastDishIdx);

  const formik = useFormik<Values>({
    initialValues: {
      searchValue: '',
    },
    onSubmit: () => {},
  });

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
  };

  useEffect(() => {
    const filtered = formik.values.searchValue
      ? dishes.filter((p) => p.name.toLowerCase().includes(formik.values.searchValue))
      : dishes;
    setFilteredDishes(filtered);
  }, [formik.values.searchValue, dishes]);

  return (
    <FilteredDishesContext.Provider
      value={{ filteredDishes, pageDishes, formik, currentPage, handlePageChange }}>
      {children}
    </FilteredDishesContext.Provider>
  );
}
